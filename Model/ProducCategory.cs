namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProducCategory")]
    public partial class ProducCategory
    {
        [Key]
        public int idProducCategory { get; set; }

        public int idProduct { get; set; }

        public int idCategory { get; set; }

        public DateTime? DateCreation { get; set; }

        public virtual Category Category { get; set; }

        public virtual Product Product { get; set; }
    }
}
