namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetail")]
    public partial class OrderDetail
    {
        [Key]
        public int idOrderDetail { get; set; }

        public int idOrder { get; set; }

        public int idProduct { get; set; }

        public decimal QuantityProduct { get; set; }

        public decimal TotalPerProduct { get; set; }

        [StringLength(150)]
        public string Detail { get; set; }

        public DateTime? DateCreation { get; set; }

        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }
    }
}
