﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace WebApplication.Controllers
{
    public class ProductsController : Controller
    {
        private DataContext db = new DataContext();
        private Category category = new Category();


        //The controller declares a class variable for an object that implements the 
        //IProductRepository interface instead of the context class         
        private IProductRepository productRepository;

        // The default constructor (without parameters) creates a new context instance
        public ProductsController()
        {
            this.productRepository = new ProductRepository(new DataContext());
        }

        // allows the caller to pass in a context instance
        public ProductsController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        // GET: Products
        public ActionResult Index()
        {
            // In CRUD methods, the repository is now called instead of the context
            var product = from p in productRepository.GetProduct()
                          select p;

            return View(product);
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // In CRUD methods, the repository is now called instead of the context
            Product product = productRepository.GetProductByID(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product, string ActionType)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (ActionType.Equals("1"))  // traditional way Entitity
                    {
                        db.Product.Add(product);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else if (ActionType.Equals("2")) // Persistent storage Entitity
                    {
                        productRepository.InsertProduct(product);
                        productRepository.Save();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // In CRUD methods, the repository is now called instead of the context Entitity
            Product product = productRepository.GetProductByID(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product, string ActionType)
        {
            if (ModelState.IsValid)
            {
                if (ActionType.Equals("1"))  // traditional way
                {
                    db.Entry(product).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index"); 
                }
                else if (ActionType.Equals("2")) // Persistent storage Entitity
                {
                    productRepository.InsertProduct(product);
                    productRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = productRepository.GetProductByID(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                // Persistent storage Entitity
                var product = productRepository.GetProductByID(id);
                productRepository.DeleteProduct(id);
                productRepository.Save();

            }
            catch (DataException)
            {
                ModelState.AddModelError(string.Empty, "Unable to delete the product. Try again, and if the problem persists contact your system administrator.");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            productRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}
