﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ProducCategoriesController : Controller
    {
        private DataContext db = new DataContext();
        
        // GET: ProducCategories
        public ActionResult Index()
        {
            var producCategory = db.ProducCategory.Include(p => p.Category).Include(p => p.Product);
            return View(producCategory.ToList());
        }

        // GET: ProducCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducCategory producCategory = db.ProducCategory.Find(id);
            if (producCategory == null)
            {
                return HttpNotFound();
            }
            return View(producCategory);
        }

        // GET: ProducCategories/Create
        public ActionResult Create()
        {
            ViewBag.idCategory = new SelectList(db.Category, "idCategory", "Name");
            ViewBag.idProduct = new SelectList(db.Product, "idProduct", "Name");

            return View();
        }

        // POST: ProducCategories/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProducCategory producCategory, string ActionType)
        {
            if (ActionType.Equals("1"))
            {
                if (ModelState.IsValid)
                {
                    db.ProducCategory.Add(producCategory);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else if (ActionType.Equals("2"))
            {

            }

            ViewBag.idCategory = new SelectList(db.Category, "idCategory", "Name", producCategory.idCategory);
            ViewBag.idProduct = new SelectList(db.Product, "idProduct", "Name", producCategory.idProduct);
            return View(producCategory);
        }

        // GET: ProducCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducCategory producCategory = db.ProducCategory.Find(id);
            if (producCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCategory = new SelectList(db.Category, "idCategory", "Name", producCategory.idCategory);
            ViewBag.idProduct = new SelectList(db.Product, "idProduct", "Name", producCategory.idProduct);
            return View(producCategory);
        }

        // POST: ProducCategories/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idProducCategory,idProduct,idCategory,DateCreation")] ProducCategory producCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idCategory = new SelectList(db.Category, "idCategory", "Name", producCategory.idCategory);
            ViewBag.idProduct = new SelectList(db.Product, "idProduct", "Name", producCategory.idProduct);
            return View(producCategory);
        }

        // GET: ProducCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProducCategory producCategory = db.ProducCategory.Find(id);
            if (producCategory == null)
            {
                return HttpNotFound();
            }
            return View(producCategory);
        }

        // POST: ProducCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProducCategory producCategory = db.ProducCategory.Find(id);
            db.ProducCategory.Remove(producCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
