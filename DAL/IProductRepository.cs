﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IProductRepository : IDisposable
    {

        IEnumerable<Product> GetProduct();

        Product GetProductByID(int? productId);

        void InsertProduct(Product product);

        void DeleteProduct(int productID);

        void UpdateProduct(Product product);

        void Save();
    }
}
