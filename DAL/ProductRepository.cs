﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL
{
    public class ProductRepository : IProductRepository, IDisposable
    {
        private DataContext db = new DataContext();

        public ProductRepository(DataContext db)
        {
            this.db = db;
        }

        public IEnumerable<Product> GetProduct()
        {
            return db.Product.ToList();
        }

        public Product GetProductByID(int? productId)
        {
            return db.Product.Find(productId);
        }

        public void InsertProduct(Product product)
        {
            db.Product.Add(product);
        }

        public void DeleteProduct(int productID)
        {
            Product product = db.Product.Find(productID);
            db.Product.Remove(product);
        }

        public void UpdateProduct(Product product)
        {
            db.Entry(product).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}
